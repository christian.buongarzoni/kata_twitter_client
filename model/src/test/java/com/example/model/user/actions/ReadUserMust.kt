package com.example.model.user.actions

import com.example.model.shared.domain.ActionResult
import com.example.model.user.domain.Nickname
import com.example.model.user.domain.UserObjectMother
import com.example.model.user.domain.Users
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.last
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test
import org.mockito.Mockito.*

@ExperimentalCoroutinesApi
internal class ReadUserMust {
    private val users = mock(Users::class.java)
    private val readUser = ReadUser(users)

    private val user = UserObjectMother.randomNewUser()
    private val nickname = user.nickname.value

    @Test
    fun `emit loading as first element`() = runTest {
        assertTrue(readUser(nickname).first() is ActionResult.Loading)
    }

    @Test
    fun `call users Read whit expected Nickname`() = runTest {
        readUser(nickname).last()

        verify(users, times(1)).read(Nickname(nickname))
    }

    @Test
    fun `emit error when read user from users fails`() = runTest {
        val errorMessage = "error getting user"
        `when`(users.read(user.nickname)).thenReturn(Result.failure(RuntimeException(errorMessage)))

        val last = readUser(nickname).last()

        assertTrue(last is ActionResult.Error)
        assertEquals(errorMessage, last.message)
    }

    @Test
    fun `emit user when it exists`() = runTest {
        `when`(users.read(user.nickname)).thenReturn(Result.success(user))

        val last = readUser(nickname).last()

        assertTrue(last is ActionResult.Success)
        assertEquals(user, last.data)
    }
}
