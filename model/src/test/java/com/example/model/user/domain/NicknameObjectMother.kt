package com.example.model.user.domain

import com.example.model.shared.domain.StringObjectMother

internal class NicknameObjectMother {
    companion object {
        fun random() = Nickname(StringObjectMother.lowercaseRandom())
    }
}
