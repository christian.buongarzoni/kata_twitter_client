package com.example.model.user.actions

import com.example.model.shared.domain.ActionResult
import com.example.model.shared.domain.Result
import com.example.model.user.domain.NicknameObjectMother
import com.example.model.user.domain.UserPreferences
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test
import org.mockito.Mockito.*

@ExperimentalCoroutinesApi
internal class ReadLoggedUserMust {

    private val userPreferences = mock(UserPreferences::class.java)
    private val readLoggedUser = ReadLoggedUser(userPreferences)

    private val nickname = NicknameObjectMother.random().value

    @Test
    fun `return success when there is a nickname saved in preferences`() = runTest {
        `when`(userPreferences.readLoggedUserNickname()).thenReturn(Result.Success(nickname))
        val result = readLoggedUser()
        assertTrue(result is ActionResult.Success)
        assertEquals(nickname, result.data)
    }

    @Test
    fun `return error when reading preferences fail`() = runTest {
        `when`(userPreferences.readLoggedUserNickname()).thenReturn(Result.Error("any error"))
        val result = readLoggedUser()
        assertTrue(result is ActionResult.Error)
        assertEquals("there isn't a logged user", result.message)
    }
}
