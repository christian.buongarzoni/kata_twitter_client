package com.example.model.user.actions

import com.example.model.shared.domain.StringObjectMother
import com.example.model.shared.domain.ActionResult
import com.example.model.user.domain.Name
import com.example.model.user.domain.Nickname
import com.example.model.user.domain.User
import com.example.model.user.domain.Users
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.last
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.*

@ExperimentalCoroutinesApi
class CreateUserMust {
    private val users = mock(Users::class.java)
    private val createUser = CreateUser(users)

    private val nickname = StringObjectMother.lowercaseRandom()
    private val name = StringObjectMother.lowercaseRandom()
    private val newUser = User(Nickname(nickname), Name(name))

    @Before
    fun setup() {
        reset(users)
    }

    @Test
    fun `emit loading as first element`() = runTest {
        assertTrue(createUser(nickname, name).first() is ActionResult.Loading)
    }

    @Test
    fun `emit error when nickname availability endpoint fails`() = runTest {
        val errorMessage = "error in nickname availability endpoint"
        `when`(users.isNicknameAvailable(Nickname(nickname))).thenReturn( Result.failure(RuntimeException(errorMessage)) )

        val last = createUser(nickname, name).last()

        assertTrue(last is ActionResult.Error)
        assertEquals(errorMessage, last.message)
    }

    @Test
    fun `emit error when nickname is not available`() = runTest {
        val errorMessage = "User already exists"
        `when`(users.isNicknameAvailable(Nickname(nickname))).thenReturn( Result.success(false) )

        val last = createUser(nickname, name).last()

        assertTrue(last is ActionResult.Error)
        assertEquals(errorMessage, last.message)
    }

    @Test
    fun `emit error when create user endpoint fails`() = runTest {
        val errorMessage = "error in create user endpoint"
        `when`(users.isNicknameAvailable(Nickname(nickname))).thenReturn( Result.success(true) )
        `when`(users.create(newUser)).thenReturn( Result.failure(RuntimeException(errorMessage)) )

        val last = createUser(nickname, name).last()

        assertTrue(last is ActionResult.Error)
        assertEquals(errorMessage, last.message)
    }

    @Test
    fun `emit Success when new user is created`() = runTest {
        `when`(users.isNicknameAvailable(Nickname(nickname))).thenReturn( Result.success(true) )
        `when`(users.create(newUser)).thenReturn( Result.success(Unit) )

        val last = createUser(nickname, name).last()

        assertTrue(last is ActionResult.Success)
    }
}
