package com.example.model.user.domain

import com.example.model.shared.domain.StringObjectMother

internal class NameObjectMother {
    companion object {
        fun random() = Name(StringObjectMother.lowercaseRandom())
    }
}
