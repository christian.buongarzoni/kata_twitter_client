package com.example.model.user.actions

import com.example.model.shared.domain.ActionResult
import com.example.model.user.domain.Nickname
import com.example.model.user.domain.NicknameObjectMother
import com.example.model.user.domain.Users
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test
import org.mockito.Mockito.*

@ExperimentalCoroutinesApi
internal class SignInMust {

    private val users = mock(Users::class.java)
    private val signIn = SignIn(users)

    private val nickname = NicknameObjectMother.random().value

    @Test
    fun `return success when the nickname is not available`() = runTest {
        `when`(users.isNicknameAvailable(Nickname(nickname))).thenReturn(Result.success(false))
        assertTrue(signIn(nickname) is ActionResult.Success)
    }

    @Test
    fun `return error when the nickname is available`() = runTest {
        `when`(users.isNicknameAvailable(Nickname(nickname))).thenReturn(Result.success(true))
        val result = signIn(nickname)
        assertTrue(result is ActionResult.Error)
        assertEquals("nickname is available", result.message)
    }

    @Test
    fun `return error when there is a problem with the repository`() = runTest {
        `when`(users.isNicknameAvailable(Nickname(nickname)))
            .thenReturn(Result.failure(RuntimeException("")))
        val result = signIn(nickname)
        assertTrue(result is ActionResult.Error)
        assertEquals("error in repository", result.message)
    }
}
