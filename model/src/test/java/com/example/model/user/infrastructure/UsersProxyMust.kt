package com.example.model.user.infrastructure

import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.Assert.*
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import retrofit2.HttpException
import retrofit2.Response

@ExperimentalCoroutinesApi
internal class UsersProxyMust {
    private val client = mock(UsersClient::class.java)
    private val users = UsersProxy(client)

    private val nickname = com.example.model.user.domain.NicknameObjectMother.random()
    private val newUser = com.example.model.user.domain.UserObjectMother.randomNewUser()
    private val existingUser = com.example.model.user.domain.UserObjectMother.randomNewUser()

    @Test
    fun `return True when field availability is {AVAILABLE}`() = runTest {
        val nicknameAvailabilityDto = NicknameAvailabilityDto(availability = "AVAILABLE")
        `when`(client.getAvailability(nickname.value)).thenReturn(nicknameAvailabilityDto)

        val result = users.isNicknameAvailable(nickname).getOrNull()

        assertTrue(result!!)
    }

    @Test
    fun `return False when field availability is not {AVAILABLE}`() = runTest {
        val nicknameAvailabilityDto = NicknameAvailabilityDto(availability = "NOT-AVAILABLE")
        `when`(client.getAvailability(nickname.value)).thenReturn(nicknameAvailabilityDto)

        val result = users.isNicknameAvailable(nickname).getOrNull()

        assertFalse(result!!)
    }

    @Test
    fun `return failure when availability endpoint response is not 2xx`() = runTest {
        val response = makeBadRetrofitResponse()
        `when`(client.getAvailability(nickname.value)).thenThrow(HttpException(response))

        val result = users.isNicknameAvailable(nickname)

        assertTrue(result.isFailure)
        assertEquals("nickname check failure code : 502", result.exceptionOrNull()!!.message)
    }

    @Test
    fun `return success when user is created successfully`() = runTest {
        `when`(client.putUser(newUser.nickname.value, newUser.name.value)).thenReturn(Unit)

        val result = users.create(newUser)

        assertTrue(result.isSuccess)
    }

    @Test
    fun `return failure when put user endpoint response is not 2xx`() = runTest {
        val response = makeBadRetrofitResponse()
        `when`(client.putUser(newUser.nickname.value, newUser.name.value)).thenThrow(HttpException(response))

        val result = users.create(newUser)

        assertTrue(result.isFailure)
        assertEquals("user registration failure code : 502", result.exceptionOrNull()!!.message)
    }

    @Test
    fun `return success when user is retrieved from get user endpoint`() = runTest {
        val userDto = existingUserToUserDto()
        `when`(client.getUser(existingUser.nickname.value)).thenReturn(userDto)

        val result = users.read(existingUser.nickname)

        assertTrue(result.isSuccess)
        assertEquals(existingUser, result.getOrNull())
    }

    @Test
    fun `return failure when get user endpoint response is not 2xx`() = runTest {
        val response = makeBadRetrofitResponse()
        `when`(client.getUser(existingUser.nickname.value)).thenThrow(HttpException(response))

        val result = users.read(existingUser.nickname)

        assertTrue(result.isFailure)
        assertEquals("retrieving user data failure - code : 502", result.exceptionOrNull()!!.message)
    }

    private fun makeBadRetrofitResponse() = Response.error<String>(
        502,
        "{\"error\":[\"bad gateway\"]}"
            .toResponseBody("application/json".toMediaTypeOrNull())
    )

    private fun existingUserToUserDto() = UserDto(
        nickname = existingUser.nickname.value,
        name = existingUser.name.value,
        tweets = existingUser.tweets.map { it.text },
    )
}
