package com.example.model.user.domain

internal class UserObjectMother {
    companion object {
        fun randomNewUser() = User(
            nickname = NicknameObjectMother.random(),
            name = NameObjectMother.random(),
        )
    }
}
