package com.example.model.shared.domain

import org.junit.Assert.assertNotEquals
import org.junit.Assert.assertTrue
import org.junit.Test
import kotlin.random.Random

object StringObjectMother {
    private val ALPHABETIC_CHARACTERS : List<Char> = ('a'..'z') + ('A'..'Z')
    private val LOWERCASE_CHARACTERS : List<Char> = ('a'..'y') + 'z'
    private val NUMERIC_CHARACTERS : List<Char> = ('0'..'8') + '9'

    fun alphabeticRandom(size: Int = 8) = stringRandomizer(size, ALPHABETIC_CHARACTERS)

    fun lowercaseRandom(size: Int = 8) = stringRandomizer(size, LOWERCASE_CHARACTERS)

    fun numericRandom(size: Int = 8) = stringRandomizer(size, NUMERIC_CHARACTERS)

    private fun stringRandomizer(size: Int, characters: List<Char>) = (1..size)
        .map { Random.nextInt(0, characters.size) }
        .map(characters::get)
        .joinToString("")
}

internal class StringObjectMotherMust{

    @Test
    fun `generate a String with alphabetic characters when called alphabeticRandom`() {
        val regex = """^[a-zA-Z]*${'$'}""".toRegex()
        val randomString = StringObjectMother.alphabeticRandom(50)
        assertTrue(randomString.matches(regex))
    }

    @Test
    fun `generate different Strings when called alphabeticRandom with same size`() {
        val firstRandomString = StringObjectMother.alphabeticRandom(5)
        val secondRandomString = StringObjectMother.alphabeticRandom(5)
        assertNotEquals(firstRandomString, secondRandomString)
    }

    @Test
    fun `generate a String with lowercase characters when called lowercaseRandom`() {
        val regex = """^[a-z]*${'$'}""".toRegex()
        val randomString = StringObjectMother.lowercaseRandom(50)
        assertTrue(randomString.matches(regex))
    }

    @Test
    fun `generate different Strings when called lowercaseRandom with same size`() {
        val firstRandomString = StringObjectMother.lowercaseRandom(5)
        val secondRandomString = StringObjectMother.lowercaseRandom(5)
        assertNotEquals(firstRandomString, secondRandomString)
    }

    @Test
    fun `generate a String with numeric characters when called numericRandom`() {
        val regex = """^[0-9]*${'$'}""".toRegex()
        val randomString = StringObjectMother.numericRandom(50)
        assertTrue(randomString.matches(regex))
    }

    @Test
    fun `generate different Strings when called numericRandom with same size`() {
        val firstRandomString = StringObjectMother.numericRandom(5)
        val secondRandomString = StringObjectMother.numericRandom(5)
        assertNotEquals(firstRandomString, secondRandomString)
    }
}
