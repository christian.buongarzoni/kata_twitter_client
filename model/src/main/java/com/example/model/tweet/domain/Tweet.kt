package com.example.model.tweet.domain

data class Tweet(val text: String)
