package com.example.model.di

import com.example.model.user.infrastructure.UsersClient
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
@InstallIn(SingletonComponent::class)
object ClientModule {

    @Provides
    fun usersClient(
        retrofit: Retrofit
    ): UsersClient = retrofit.create(UsersClient::class.java)


    @Provides
    fun retrofit(): Retrofit {
        val client = OkHttpClient.Builder().build()

        return Retrofit.Builder()
            .baseUrl("https://proxy.etermax.com/api/kata_chris/")
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().setLenient().create()))
            .client(client)
            .build()
    }
}
