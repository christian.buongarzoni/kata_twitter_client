package com.example.model.di

import android.content.Context
import com.example.model.user.domain.UserPreferences
import com.example.model.user.infrastructure.UserDataStorePreferences
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object PreferencesModule {
    @Singleton
    @Provides
    fun sharedPreferences(
        @ApplicationContext appContext: Context
    ) : UserPreferences = UserDataStorePreferences(appContext)
}
