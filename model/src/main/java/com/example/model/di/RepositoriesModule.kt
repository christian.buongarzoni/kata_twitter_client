package com.example.model.di

import com.example.model.user.domain.Users
import com.example.model.user.infrastructure.UsersProxy
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoriesModule {
    @Binds
    @Singleton
    internal abstract fun bindUsers(usersProxy: UsersProxy): Users
}
