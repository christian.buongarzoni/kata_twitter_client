package com.example.model.shared.domain

sealed class ActionResult<T>(
    val data: T? = null,
    val message: String? = null
) {
    class Success<T>(data: T?) : ActionResult<T>(data = data)

    class Error<T>(message: String?, data: T? = null) : ActionResult<T>(data = data, message = message)

    class Loading<T>(data: T? = null) : ActionResult<T>(data = data)
}
