package com.example.model.user.domain

data class Nickname(val value: String)
