package com.example.model.user.infrastructure

data class NicknameAvailabilityDto(val availability: String)