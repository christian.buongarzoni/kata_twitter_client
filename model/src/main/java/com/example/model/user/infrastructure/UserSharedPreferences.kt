package com.example.model.user.infrastructure

import android.content.Context
import android.content.SharedPreferences
import com.example.model.user.domain.UserPreferences
import com.example.model.shared.domain.Result

class UserSharedPreferences(context: Context) : UserPreferences {
    companion object {
        private const val PREFERENCES_NAME = "kataTwitterClient.userSharedPreferences"
        private const val NICKNAME_KEY = "userNickname"
    }

    private val storage: SharedPreferences = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE)

    override suspend fun createLoggedUser(nickname: String) : Result<Unit> {
        with(storage.edit()) {
            putString(NICKNAME_KEY, nickname)
            commit()
        }
        return Result.Success(Unit)
    }

    override suspend fun readLoggedUserNickname() : Result<String> {
        val nickname = storage.getString(NICKNAME_KEY, "")
        if(nickname.isNullOrEmpty()) {
            return Result.Error(message = "there is no logged user")
        }
        return Result.Success(nickname)
    }
}
