package com.example.model.user.actions

import com.example.model.shared.domain.ActionResult
import com.example.model.user.domain.Nickname
import com.example.model.user.domain.Users
import javax.inject.Inject

class SignIn @Inject constructor(private val users: Users) {
    suspend operator fun invoke(nickname: String): ActionResult<Unit> {
        users.isNicknameAvailable(nickname = Nickname(nickname))
            .onFailure { return ActionResult.Error("error in repository") }
            .onSuccess { if(!it) { return ActionResult.Success(Unit) } }
        return ActionResult.Error("nickname is available")
    }
}
