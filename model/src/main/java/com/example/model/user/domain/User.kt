package com.example.model.user.domain

import com.example.model.tweet.domain.Tweet

data class User(
    val nickname: Nickname,
    val name: Name,
    val tweets: List<Tweet> = listOf(),
)
