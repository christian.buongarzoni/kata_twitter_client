package com.example.model.user.actions

import com.example.model.shared.domain.ActionResult
import com.example.model.user.domain.Name
import com.example.model.user.domain.Nickname
import com.example.model.user.domain.User
import com.example.model.user.domain.Users
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class CreateUser @Inject constructor(private val users: Users) {
    suspend operator fun invoke(nickname: String, name: String): Flow<ActionResult<Unit>> = flow {
        emit(ActionResult.Loading())

        users.isNicknameAvailable(nickname = Nickname(nickname))
            .onFailure {
                emit(ActionResult.Error(message = it.message))
                return@flow
            }.onSuccess { nicknameIsAvailable ->
                if (!nicknameIsAvailable) {
                    emit(ActionResult.Error(message = "User already exists"))
                    return@flow
                }
            }

        val newUser = User(nickname = Nickname(nickname), name = Name(name))
        users.create(newUser).onSuccess {
            emit(ActionResult.Success(Unit))
        }.onFailure {
            emit(ActionResult.Error(message = it.message))
        }
    }
}
