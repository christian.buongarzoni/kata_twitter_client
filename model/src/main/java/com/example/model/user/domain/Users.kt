package com.example.model.user.domain

interface Users {
    suspend fun isNicknameAvailable(nickname: Nickname): Result<Boolean>
    suspend fun create(user: User): Result<Unit>
    suspend fun read(nickname: Nickname): Result<User>
}
