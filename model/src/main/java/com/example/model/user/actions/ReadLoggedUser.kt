package com.example.model.user.actions

import com.example.model.shared.domain.ActionResult
import com.example.model.shared.domain.Result
import com.example.model.user.domain.UserPreferences
import javax.inject.Inject

class ReadLoggedUser @Inject constructor(private val userPreferences: UserPreferences) {
    suspend operator fun invoke() : ActionResult<String> {
        return when(val result = userPreferences.readLoggedUserNickname()) {
            is Result.Error -> {
                ActionResult.Error("there isn't a logged user")
            }
            is Result.Success -> {
                ActionResult.Success(result.data)
            }
        }
    }
}
