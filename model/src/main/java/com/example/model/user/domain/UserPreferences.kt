package com.example.model.user.domain

import com.example.model.shared.domain.Result

interface UserPreferences {
    suspend fun createLoggedUser(nickname: String) : Result<Unit>
    suspend fun readLoggedUserNickname() : Result<String>
}
