package com.example.model.user.infrastructure

import retrofit2.http.GET
import retrofit2.http.PUT
import retrofit2.http.Path

interface UsersClient {

    @GET("nicknames/availability/{nickname}")
    suspend fun getAvailability(@Path("nickname") nickname: String): NicknameAvailabilityDto

    @PUT("users/{nickname}/{name}")
    suspend fun putUser(
        @Path("nickname") nickname: String,
        @Path("name") name: String,
    )

    @GET("user/{nickname}")
    suspend fun getUser(@Path("nickname") nickname: String): UserDto
}
