package com.example.model.user.infrastructure

import com.example.model.tweet.domain.Tweet
import com.example.model.user.domain.Name
import com.example.model.user.domain.Nickname
import com.example.model.user.domain.User

data class UserDto(
    val nickname: String,
    val name: String,
    val tweets: List<String>,
) {
    fun toModel(): User {
        return User(
            nickname = Nickname(nickname),
            name = Name(name),
            tweets = tweets.map { Tweet(it) },
        )
    }
}
