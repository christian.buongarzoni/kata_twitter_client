package com.example.model.user.infrastructure

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import com.example.model.shared.domain.Result
import com.example.model.user.domain.UserPreferences
import kotlinx.coroutines.flow.first
import javax.inject.Inject

class UserDataStorePreferences @Inject constructor(context: Context) : UserPreferences {
    companion object {
        private const val PREFERENCES_NAME = "kataTwitterClient.userDataStorePreferences"
        private val NICKNAME_KEY = stringPreferencesKey("userNickname")
    }

    private val Context.userDataStorePreferences: DataStore<Preferences> by preferencesDataStore(
        name = PREFERENCES_NAME
    )
    private val dataStore = context.userDataStorePreferences

    override suspend fun createLoggedUser(nickname: String): Result<Unit> {
        dataStore.edit { it[NICKNAME_KEY] = nickname }
        return Result.Success(Unit)
    }

    override suspend fun readLoggedUserNickname(): Result<String> {
        val nickname = dataStore.data.first()[NICKNAME_KEY]
        if(nickname.isNullOrEmpty()) {
            return Result.Error(message = "there is no logged user")
        }
        return Result.Success(nickname)
    }
}
