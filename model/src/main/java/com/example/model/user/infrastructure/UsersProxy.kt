package com.example.model.user.infrastructure

import com.example.model.user.domain.Nickname
import com.example.model.user.domain.User
import com.example.model.user.domain.Users
import retrofit2.HttpException
import java.lang.RuntimeException
import javax.inject.Inject

class UsersProxy @Inject constructor(private val client: UsersClient) : Users {

    override suspend fun isNicknameAvailable(nickname: Nickname): Result<Boolean> {
        val response = try {
            client.getAvailability(nickname.value)
        } catch (httpException: HttpException) {
            return Result.failure(RuntimeException("nickname check failure code : ${httpException.code()}"))
        }

        return Result.success(value = response.availability == "AVAILABLE")
    }

    override suspend fun create(user: User): Result<Unit> {
        return try {
            client.putUser(
                nickname = user.nickname.value,
                name = user.name.value,
            )
            Result.success(Unit)
        } catch (httpException: HttpException) {
            Result.failure(RuntimeException("user registration failure code : ${httpException.code()}"))
        }
    }

    override suspend fun read(nickname: Nickname): Result<User> {
        return try {
            val user = client.getUser(nickname.value).toModel()
            Result.success(user)
        } catch (httpException: HttpException) {
            Result.failure(RuntimeException("retrieving user data failure - code : ${httpException.code()}"))
        }
    }
}
