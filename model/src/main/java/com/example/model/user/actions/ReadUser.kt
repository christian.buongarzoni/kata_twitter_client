package com.example.model.user.actions

import com.example.model.shared.domain.ActionResult
import com.example.model.user.domain.Nickname
import com.example.model.user.domain.User
import com.example.model.user.domain.Users
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class ReadUser @Inject constructor(private val users: Users) {

    suspend operator fun invoke(nickname: String): Flow<ActionResult<User>> = flow {
        emit(ActionResult.Loading())
        users.read(Nickname(nickname))
            .onFailure { emit(ActionResult.Error(message = it.message)) }
            .onSuccess { emit(ActionResult.Success(it)) }
    }
}
