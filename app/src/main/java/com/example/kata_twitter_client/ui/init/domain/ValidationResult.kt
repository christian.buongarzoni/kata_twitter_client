package com.example.kata_twitter_client.ui.init.domain

data class ValidationResult(
    val isSuccessful: Boolean,
    val errorMessage: String? = null,
)
