package com.example.kata_twitter_client.ui.init.domain

import javax.inject.Inject

class ValidateNickname @Inject constructor() : Validator {

    companion object {
        private val REGEX = """^[a-z0-9_.]*${'$'}""".toRegex()
        private const val EMPTY_NICKNAME_ERROR_MESSAGE = "Please add a nickname"
        private const val INVALID_CHARACTERS_ERROR_MESSAGE =
            "Nickname must contain only lowercase letters, numbers, dots and underscores"
        private const val NICKNAME_MAX_SIZE = 21
        private const val OVERSIZE_ERROR_MESSAGE = "Nickname must be up to 21 characters"
    }

    override operator fun invoke(string: String): ValidationResult {
        return if (string.isEmpty()) {
            emptyNicknameErrorResult()
        } else if (string.hasInvalidCharacters()) {
            invalidCharactersErrorResult()
        } else if (string.isOversized()) {
            overSizedNicknameErrorResult()
        } else {
            successfulResult()
        }
    }

    private fun String.hasInvalidCharacters() = !this.matches(REGEX)
    private fun String.isOversized() = this.length > NICKNAME_MAX_SIZE

    private fun emptyNicknameErrorResult(): ValidationResult = ValidationResult(
        isSuccessful = false,
        errorMessage = EMPTY_NICKNAME_ERROR_MESSAGE,
    )

    private fun invalidCharactersErrorResult(): ValidationResult = ValidationResult(
        isSuccessful = false,
        errorMessage = INVALID_CHARACTERS_ERROR_MESSAGE,
    )

    private fun overSizedNicknameErrorResult(): ValidationResult = ValidationResult(
        isSuccessful = false,
        errorMessage = OVERSIZE_ERROR_MESSAGE,
    )

    private fun successfulResult(): ValidationResult = ValidationResult(
        isSuccessful = true,
    )
}
