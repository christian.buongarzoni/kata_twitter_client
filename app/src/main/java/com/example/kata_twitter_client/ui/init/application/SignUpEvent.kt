package com.example.kata_twitter_client.ui.init.application

sealed class SignUpEvent {
    data class NicknameChanged(val nickname: String): SignUpEvent()
    data class NameChanged(val name: String): SignUpEvent()
    object OnSubmit: SignUpEvent()
}
