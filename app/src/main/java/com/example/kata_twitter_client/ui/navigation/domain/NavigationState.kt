package com.example.kata_twitter_client.ui.navigation.domain

sealed class NavigationState {

    object Idle : NavigationState()

    data class NavigateToRoute(val route: String) :
        NavigationState()

    data class PopToRoute(val staticRoute: String) :
        NavigationState()

    object NavigateUp : NavigationState()
}
