package com.example.kata_twitter_client.ui.user.application

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.kata_twitter_client.ui.navigation.domain.RouteNavigator
import com.example.kata_twitter_client.ui.user.views.UserProfileRoute
import com.example.model.shared.domain.ActionResult
import com.example.model.user.actions.ReadUser
import com.example.model.user.domain.User
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

interface UserProfileViewModel {
    val nickname: String
    val userDataState: State<UserDataState>
    val tweets: State<List<String>>

    fun getUserData(nickname: String)
}

class PreviewUserProfileViewModel: UserProfileViewModel {
    override val nickname = "Chris"
    override val userDataState: State<UserDataState> =
        mutableStateOf(UserDataState(isUserDataRetrieved = true))
    override val tweets: State<List<String>> = mutableStateOf(
        listOf(
            "Any tweet",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            "Any tweet",
        )
    )

    override fun getUserData(nickname: String) { /* ignored */ }
}

@HiltViewModel
class UserProfileViewModelImpl @Inject constructor(
    private val routeNavigator: RouteNavigator,
    savedStateHandle: SavedStateHandle,
    private val readUser: ReadUser,
): ViewModel(), UserProfileViewModel, RouteNavigator by routeNavigator {

    private var _userDataState = mutableStateOf(UserDataState(isLoading = true))
    override val userDataState: State<UserDataState> = _userDataState
    override val nickname = UserProfileRoute.getNicknameFrom(savedStateHandle)
    private var _tweets = mutableStateOf<List<String>>(listOf())
    override val tweets: State<List<String>> = _tweets

    init {
        getUserData(nickname)
    }

    override fun getUserData(nickname: String) {
        viewModelScope.launch {
            readUser(nickname).collect { result ->
                when (result) {
                    is ActionResult.Loading -> {
                        _userDataState.value = UserDataState(isLoading = true)
                    }
                    is ActionResult.Error -> {
                        _userDataState.value = UserDataState(errorMessage = result.message)
                    }
                    is ActionResult.Success -> {
                        updateUserData(result.data!!)
                        _userDataState.value = UserDataState(isUserDataRetrieved = true)
                    }
                }
            }
        }
    }

    private fun updateUserData(user: User) {
        _tweets.value = user.tweets.map { it.text }
    }
}
