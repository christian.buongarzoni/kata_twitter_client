package com.example.kata_twitter_client.ui.init.views

import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.kata_twitter_client.R
import com.example.kata_twitter_client.ui.init.application.*
import com.example.kata_twitter_client.ui.navigation.domain.NavRoute
import com.example.kata_twitter_client.ui.theme.Kata_Twitter_ClientTheme
import com.example.kata_twitter_client.ui.utils.views.AppAlertDialog
import com.example.kata_twitter_client.ui.utils.views.AppButton
import com.example.kata_twitter_client.ui.utils.views.TwitterTopAppBar
import com.example.kata_twitter_client.ui.utils.views.placeholders.NameInputPlaceholder
import com.example.kata_twitter_client.ui.utils.views.placeholders.NicknameInputPlaceholder

object SignUpRoute : NavRoute<SignUpViewModelImpl> {

    override val route = "sign_up/"

    @Composable
    override fun viewModel(): SignUpViewModelImpl = hiltViewModel()

    @Composable
    override fun Content(viewModel: SignUpViewModelImpl) = SignUpScreen(viewModel)
}

@Composable
private fun SignUpScreen(viewModel: SignUpViewModel) {
    Scaffold(
        topBar = { TwitterTopAppBar(title = stringResource(id = R.string.top_app_bar_label)) },
    ) { paddingValues ->
        val ignoring = paddingValues
        ScreenContent(viewModel = viewModel)
    }
}

@Composable
private fun ScreenContent(viewModel: SignUpViewModel) {
    Box(
        modifier = Modifier.padding(25.dp),
    ) {
        Column(
            modifier = Modifier.fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center,
        ) {
            NicknamePlaceholder(viewModel = viewModel)
            Spacer(modifier = Modifier.height(20.dp))

            NamePlaceholder(viewModel = viewModel)
            Spacer(modifier = Modifier.height(35.dp))

            SignUpPlaceholder(viewModel = viewModel)
        }
    }
}

@Composable
private fun NicknamePlaceholder(viewModel: SignUpViewModel) {
    val nickname = viewModel.nickname.value

    NicknameInputPlaceholder(
        modifier = Modifier.fillMaxWidth(),
        text = nickname,
        onValueChange = { viewModel.onEvent(SignUpEvent.NicknameChanged(it)) }
    )

    NicknameError(viewModel = viewModel)
}

@Composable
private fun NamePlaceholder(viewModel: SignUpViewModel) {
    val name = viewModel.name.value

    NameInputPlaceholder(
        modifier = Modifier.fillMaxWidth(),
        text = name,
        onValueChange = { viewModel.onEvent(SignUpEvent.NameChanged(it)) }
    )

    NameError(viewModel = viewModel)
}

@Composable
private fun SignUpPlaceholder(viewModel: SignUpViewModel) {
    val registrationState = viewModel.registrationState.value

    if (registrationState.isLoading) {
        CircularProgressIndicator()
    } else {
        SignUpButton(viewModel = viewModel)
    }

    AlertSubmitError(errorMessage = registrationState.errorMessage)
}

@Composable
private fun SignUpButton(viewModel: SignUpViewModel) {
    AppButton(
        text = stringResource(id = R.string.sign_me_up_label),
        modifier = Modifier.fillMaxWidth(),
        onClick = { viewModel.onEvent(SignUpEvent.OnSubmit) },
    )
}

@Composable
private fun NicknameError(viewModel: SignUpViewModel) {
    val error = viewModel.nicknameError.value
    error?.let { TextFieldError(textError = it) }
}

@Composable
private fun NameError(viewModel: SignUpViewModel) {
    val error = viewModel.nameError.value
    error?.let { TextFieldError(textError = it) }
}

@Composable
private fun TextFieldError(textError: String) {
    Row(modifier = Modifier.fillMaxWidth()) {
        Spacer(modifier = Modifier.width(16.dp))
        Text(
            text = textError,
            modifier = Modifier.fillMaxWidth(),
            style = LocalTextStyle.current.copy(color = MaterialTheme.colors.error),
        )
    }
}

@Composable
private fun AlertSubmitError(errorMessage: String?) {
    errorMessage?.let {
        AppAlertDialog(
            title = "error :(",
            errorMessage = it,
        )
    }
}

@Composable
@Preview(showBackground = true)
private fun PreviewLightTheme() {
    Kata_Twitter_ClientTheme(darkTheme = false) {
        SignUpScreen(PreviewSignUpViewModel())
    }
}

@Composable
@Preview(showBackground = true)
private fun PreviewDarkTheme() {
    Kata_Twitter_ClientTheme(darkTheme = true) {
        SignUpScreen(PreviewSignUpViewModel())
    }
}
