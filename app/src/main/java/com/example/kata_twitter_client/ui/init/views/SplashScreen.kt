package com.example.kata_twitter_client.ui.init.views

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.kata_twitter_client.R
import com.example.kata_twitter_client.ui.init.application.SplashViewModelImpl
import com.example.kata_twitter_client.ui.navigation.domain.NavRoute
import com.example.kata_twitter_client.ui.theme.Kata_Twitter_ClientTheme
import com.example.kata_twitter_client.ui.utils.views.TwitterTopAppBar

object SplashRoute : NavRoute<SplashViewModelImpl> {
    override val route = "splash/"

    @Composable
    override fun viewModel() : SplashViewModelImpl = hiltViewModel()

    @Composable
    override fun Content(viewModel: SplashViewModelImpl) = SplashScreen()
}

@Composable
@Preview(showBackground = true)
private fun PreviewLightTheme() {
    Kata_Twitter_ClientTheme(darkTheme = false) {
        SplashScreen()
    }
}

@Composable
@Preview(showBackground = true)
private fun PreviewDarkTheme() {
    Kata_Twitter_ClientTheme(darkTheme = true) {
        SplashScreen()
    }
}

@Composable
private fun SplashScreen() {
    Scaffold(
        topBar = { TwitterTopAppBar(title = stringResource(id = R.string.top_app_bar_label)) },
    ) {
        val ignored = it
        ScreenContent()
    }
}

@Composable
private fun ScreenContent() {
    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center,
    ) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            Image(
                painter = painterResource(id = R.drawable.twitter_logo),
                contentDescription = "Splash image",
            )
            CircularProgressIndicator()
        }
    }
}
