package com.example.kata_twitter_client.ui.user.application

data class UserDataState(
    val isLoading: Boolean = false,
    val isUserDataRetrieved: Boolean = false,
    val errorMessage: String? = null,
)
