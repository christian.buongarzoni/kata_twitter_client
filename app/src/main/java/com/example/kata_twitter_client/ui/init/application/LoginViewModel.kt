package com.example.kata_twitter_client.ui.init.application

import androidx.lifecycle.ViewModel
import com.example.kata_twitter_client.ui.init.views.SignUpRoute
import com.example.kata_twitter_client.ui.navigation.domain.RouteNavigator
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

interface LoginViewModel {
    fun onRegisterClicked()
}

class PreviewLoginViewModel: LoginViewModel {
    override fun onRegisterClicked() {}
}

@HiltViewModel
class LoginViewModelImpl @Inject constructor(
    private val routeNavigator: RouteNavigator,
): ViewModel(), LoginViewModel, RouteNavigator by routeNavigator {
    override fun onRegisterClicked() {
        routeNavigator.navigateToRoute(SignUpRoute.route)
    }
}
