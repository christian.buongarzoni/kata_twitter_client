package com.example.kata_twitter_client.ui.init.domain

import javax.inject.Inject

class ValidateName @Inject constructor(): Validator {

    companion object {
        private val REGEX = """^[a-zA-Z ]*${'$'}""".toRegex()
        private const val EMPTY_NAME_ERROR_MESSAGE = "Please add a name"
        private const val INVALID_CHARACTERS_ERROR_MESSAGE =
            "Name must contain only alphabetic characters and spaces"
        private const val OVERSIZE_ERROR_MESSAGE = "Name must be up to 50 characters"
        private const val NAME_MAX_SIZE = 50
    }

    override operator fun invoke(string: String): ValidationResult {
        return if (string.isBlank()) {
            emptyNameErrorResult()
        } else if (string.hasInvalidCharacters()) {
            invalidCharactersErrorResult()
        } else if (string.isOversized()) {
            overSizedNameErrorResult()
        } else successfulResult()
    }

    private fun String.hasInvalidCharacters() = !this.matches(REGEX)
    private fun String.isOversized() = this.length > NAME_MAX_SIZE

    private fun emptyNameErrorResult(): ValidationResult = ValidationResult(
        isSuccessful = false,
        errorMessage = EMPTY_NAME_ERROR_MESSAGE,
    )

    private fun invalidCharactersErrorResult(): ValidationResult = ValidationResult(
        isSuccessful = false,
        errorMessage = INVALID_CHARACTERS_ERROR_MESSAGE,
    )

    private fun overSizedNameErrorResult(): ValidationResult = ValidationResult(
        isSuccessful = false,
        errorMessage = OVERSIZE_ERROR_MESSAGE,
    )

    private fun successfulResult(): ValidationResult = ValidationResult(
        isSuccessful = true,
    )
}
