package com.example.kata_twitter_client.ui.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable

private val DarkColorPalette = darkColors(
    primary = Blue400,
    primaryVariant = Cyan200,
    secondary = Teal200,
    background = TwitterBlack,
    surface = Blue400
)

private val LightColorPalette = lightColors(
    primary = Blue400,
    primaryVariant = Cyan200,
    secondary = TwitterBlack,
    secondaryVariant = TwitterDarkGray,
    surface = Blue400
)

@Composable
fun Kata_Twitter_ClientTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    content: @Composable () -> Unit
) {
    val colors = if (darkTheme) {
        DarkColorPalette
    } else {
        LightColorPalette
    }

    MaterialTheme(
        colors = colors,
        typography = Typography,
        shapes = Shapes,
        content = content
    )
}