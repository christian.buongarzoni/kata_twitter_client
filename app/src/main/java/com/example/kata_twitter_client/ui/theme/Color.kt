package com.example.kata_twitter_client.ui.theme

import androidx.compose.ui.graphics.Color

val Teal200 = Color(0xFF03DAC5)
val Blue400 = Color(0xFF42A5F5)
val Cyan200 = Color(0xFF80DEEA)
val TwitterBlack = Color(0xFF14171A)
val TwitterDarkGray = Color(0xFF3a3d40)