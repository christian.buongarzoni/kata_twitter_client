package com.example.kata_twitter_client.ui.utils.views

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp


@Composable
fun TwitterTopAppBar(title: String){
    TopAppBar(
        backgroundColor = MaterialTheme.colors.primary,
        elevation = 0.dp
    ) {
        Text(
            modifier = Modifier.fillMaxSize().wrapContentSize(Alignment.Center),
            text = title,
            textAlign = TextAlign.Center,
            style = MaterialTheme.typography.h6,
        )
    }
}
