package com.example.kata_twitter_client.ui.init.domain

interface Validator {
    operator fun invoke(string: String): ValidationResult
}
