package com.example.kata_twitter_client.ui.init.application

data class RegistrationState(
    val isLoading: Boolean = false,
    val registrationCompleted: Boolean = false,
    val errorMessage: String? = null,
)
