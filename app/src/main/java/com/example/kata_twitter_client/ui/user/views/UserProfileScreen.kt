package com.example.kata_twitter_client.ui.user.views

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.SavedStateHandle
import androidx.navigation.NamedNavArgument
import androidx.navigation.NavType
import androidx.navigation.navArgument
import com.example.kata_twitter_client.R
import com.example.kata_twitter_client.ui.navigation.domain.NavRoute
import com.example.kata_twitter_client.ui.navigation.domain.getOrThrow
import com.example.kata_twitter_client.ui.theme.Kata_Twitter_ClientTheme
import com.example.kata_twitter_client.ui.user.application.PreviewUserProfileViewModel
import com.example.kata_twitter_client.ui.user.application.UserProfileViewModel
import com.example.kata_twitter_client.ui.user.application.UserProfileViewModelImpl
import com.example.kata_twitter_client.ui.utils.views.TwitterTopAppBar
import com.example.kata_twitter_client.ui.utils.views.animatedShimmerBrush


object UserProfileRoute : NavRoute<UserProfileViewModelImpl> {
    private const val KEY_USER_NICKNAME = "USER_NICKNAME"

    override val route = "user/{$KEY_USER_NICKNAME}/"

    fun get(nickname: String): String = route.replace("{$KEY_USER_NICKNAME}", nickname)

    fun getNicknameFrom(savedStateHandle: SavedStateHandle) =
        savedStateHandle.getOrThrow<String>(KEY_USER_NICKNAME)

    override fun getArguments(): List<NamedNavArgument> = listOf(
        navArgument(KEY_USER_NICKNAME) { type = NavType.StringType })

    @Composable
    override fun viewModel(): UserProfileViewModelImpl = hiltViewModel()

    @Composable
    override fun Content(viewModel: UserProfileViewModelImpl) = UserProfileScreen(viewModel)
}

@Composable
private fun UserProfileScreen(userProfileViewModel: UserProfileViewModel) {
    Scaffold(
        topBar = { TwitterTopAppBar(title = stringResource(id = R.string.top_app_bar_label)) },
    ) {
        val ignored = it
        ScreenContent(userProfileViewModel = userProfileViewModel)
    }
}

@Composable
private fun ScreenContent(userProfileViewModel: UserProfileViewModel) {
    Box(
        modifier = Modifier.padding(25.dp),
        contentAlignment = Alignment.TopCenter,
    ) {
        Column(
            modifier = Modifier.fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Top,
        ) {
            Row {
                Box(Modifier.clickable { }) {
                    Column(horizontalAlignment = Alignment.CenterHorizontally) {
                        Text(text = stringResource(id = R.string.followers_label))
                        Text(text = "0")
                    }
                }
                Spacer(Modifier.weight(1f))
                Box(Modifier.clickable { }) {
                    Column(horizontalAlignment = Alignment.CenterHorizontally) {
                        Text(text = stringResource(id = R.string.following_label))
                        Text(text = "0")
                    }
                }
            }
            Spacer(modifier = Modifier.height(30.dp))
            Text(
                text = "@${userProfileViewModel.nickname}",
                style = MaterialTheme.typography.h4,
            )
            Spacer(modifier = Modifier.height(15.dp))
            Text(
                text = stringResource(id = R.string.name_label),
                style = MaterialTheme.typography.h5,
            )
            Spacer(modifier = Modifier.height(30.dp))
            TweetsBox(userProfileViewModel)
        }
    }
}

@Composable
private fun TweetsBox(userProfileViewModel: UserProfileViewModel) {
    val userDataState = userProfileViewModel.userDataState.value
    Box(
        modifier = Modifier
            .fillMaxSize()
            .padding(15.dp),
        contentAlignment = Alignment.TopCenter
    ) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            Text(
                text = stringResource(id = R.string.tweets),
                style = MaterialTheme.typography.h5,
            )
            Spacer(modifier = Modifier.height(10.dp))
            if (userDataState.isLoading) {
                AddShimmerTweetBoxes()
            } else if (userDataState.isUserDataRetrieved) {
                AddTweets(userProfileViewModel)
            }
        }
    }

}

@Composable
private fun AddShimmerTweetBoxes() {
    LazyColumn(modifier = Modifier.fillMaxSize()) {
        items(count = 4) {
            Card(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(10.dp)
                    .clickable { },
                elevation = 2.dp,
            ) {
                Spacer(
                    modifier = Modifier
                        .size(124.dp)
                        .clip(MaterialTheme.shapes.medium)
                        .background(animatedShimmerBrush()),
                )
            }
        }
    }
}

@Composable
private fun AddTweets(userProfileViewModel: UserProfileViewModel) {
    val tweets = userProfileViewModel.tweets.value
    LazyColumn(modifier = Modifier.fillMaxSize()) {
        items(items = tweets) {
            TweetCard(it)
        }
    }
}

@Composable
private fun TweetCard(tweet: String) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(10.dp)
            .clickable { },
        elevation = 2.dp,
    ) {
        Text(
            modifier = Modifier.padding(20.dp),
            text = tweet,
        )
    }
}

@Composable
@Preview(showBackground = true)
private fun PreviewLightTheme() {
    Kata_Twitter_ClientTheme(darkTheme = false) {
        UserProfileScreen(PreviewUserProfileViewModel())
    }
}

@Composable
@Preview(showBackground = true)
private fun PreviewDarkTheme() {
    Kata_Twitter_ClientTheme(darkTheme = true) {
        UserProfileScreen(PreviewUserProfileViewModel())
    }
}
