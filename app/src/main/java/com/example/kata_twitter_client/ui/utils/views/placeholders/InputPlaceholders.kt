package com.example.kata_twitter_client.ui.utils.views.placeholders

import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Person
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import com.example.kata_twitter_client.R

@Composable
fun NameInputPlaceholder(
    modifier: Modifier,
    text: String,
    imeAction: ImeAction = ImeAction.Next,
    onImeAction: () -> Unit = {},
    onValueChange: (String) -> Unit = {},
) {
    InputPlaceholder(
        modifier = modifier,
        text = text,
        label = stringResource(id = R.string.name_label),
        imeAction = imeAction,
        icon = {
            Icon(
                imageVector = Icons.Filled.Person,
                contentDescription = ""
            )
               },
        onImeAction = onImeAction,
        onValueChange = onValueChange,
    )
}

@Composable
fun NicknameInputPlaceholder(
    modifier: Modifier,
    text: String,
    imeAction: ImeAction = ImeAction.Next,
    onImeAction: () -> Unit = {},
    onValueChange: (String) -> Unit = {},
) {
    InputPlaceholder(
        modifier = modifier,
        text = text,
        label = stringResource(id = R.string.nickname_label),
        imeAction = imeAction,
        icon = {
            Icon(
                imageVector = Icons.Filled.Person,
                contentDescription = ""
            )
        },
        onImeAction = onImeAction,
        onValueChange = onValueChange,
    )
}

@Composable
private fun InputPlaceholder(
    modifier: Modifier,
    text: String,
    label: String = "",
    imeAction: ImeAction = ImeAction.Next,
    icon: @Composable (() -> Unit) = {},
    onImeAction: () -> Unit = {},
    onValueChange: (String) -> Unit = {},
) {
    TextField(
        value = text,
        onValueChange = onValueChange,
        label = {
            CompositionLocalProvider(LocalContentAlpha provides ContentAlpha.medium) {
                Text(
                    text = label,
                    style = MaterialTheme.typography.body2,
                )
            }
        },
        modifier = modifier,
        trailingIcon = icon,
        textStyle = MaterialTheme.typography.body2,
        keyboardOptions = KeyboardOptions.Default.copy(imeAction = imeAction),
        keyboardActions = KeyboardActions(
            onDone = {
                onImeAction()
            }
        ),
        colors = TextFieldDefaults.textFieldColors(
            backgroundColor = Color.Transparent
        )
    )
}
