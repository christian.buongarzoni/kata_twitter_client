package com.example.kata_twitter_client.ui.init.views

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.ClickableText
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.kata_twitter_client.R
import com.example.kata_twitter_client.ui.init.application.PreviewLoginViewModel
import com.example.kata_twitter_client.ui.init.application.LoginViewModel
import com.example.kata_twitter_client.ui.init.application.LoginViewModelImpl
import com.example.kata_twitter_client.ui.navigation.domain.NavRoute
import com.example.kata_twitter_client.ui.theme.Kata_Twitter_ClientTheme
import com.example.kata_twitter_client.ui.utils.views.AppButton
import com.example.kata_twitter_client.ui.utils.views.TwitterTopAppBar
import com.example.kata_twitter_client.ui.utils.views.placeholders.NicknameInputPlaceholder

object LoginRoute : NavRoute<LoginViewModelImpl> {

    override val route = "login/"

    @Composable
    override fun viewModel(): LoginViewModelImpl = hiltViewModel()

    @Composable
    override fun Content(viewModel: LoginViewModelImpl) = LoginScreen(viewModel)
}

@Composable
private fun LoginScreen(viewModel: LoginViewModel) {
    Scaffold(
        topBar = { TwitterTopAppBar(title = stringResource(id = R.string.top_app_bar_label)) },
    ) {
        val ignored = it
        ScreenContent(viewModel)
    }
}

@Composable
private fun ScreenContent(viewModel: LoginViewModel) {
    Box(
        modifier = Modifier.padding(25.dp),
        contentAlignment = Alignment.Center,
    ) {
        Column(
            modifier = Modifier.fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center,
        ) {
            NicknameInputPlaceholder(
                modifier = Modifier.fillMaxWidth(),
                text = "",
            )
            Spacer(Modifier.height(10.dp))

            AppButton(
                text = stringResource(id = R.string.sign_in_label),
                modifier = Modifier.fillMaxWidth(),
            )
            Spacer(Modifier.height(10.dp))

            Row(verticalAlignment = Alignment.CenterVertically) {
                Text(text = stringResource(id = R.string.ask_if_have_an_account))
                Spacer(Modifier.width(10.dp))
                ClickableText(
                    style = TextStyle(
                        color = MaterialTheme.colors.onBackground,
                        fontWeight = FontWeight.Bold,
                    ),
                    text = buildAnnotatedString {
                        append(stringResource(id = R.string.sign_up_label))
                    },
                ) {
                    viewModel.onRegisterClicked()
                }
            }
        }
    }
}

@Composable
@Preview(showBackground = true)
private fun PreviewLightTheme() {
    Kata_Twitter_ClientTheme(darkTheme = false) {
        LoginScreen(PreviewLoginViewModel())
    }
}

@Composable
@Preview(showBackground = true)
private fun PreviewDarkTheme() {
    Kata_Twitter_ClientTheme(darkTheme = true) {
        LoginScreen(PreviewLoginViewModel())
    }
}
