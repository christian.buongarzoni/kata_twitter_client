package com.example.kata_twitter_client.ui.init.application

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.kata_twitter_client.ui.init.views.LoginRoute
import com.example.kata_twitter_client.ui.navigation.domain.RouteNavigator
import com.example.kata_twitter_client.ui.user.views.UserProfileRoute
import com.example.model.shared.domain.ActionResult
import com.example.model.user.actions.ReadLoggedUser
import com.example.model.user.actions.SignIn
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SplashViewModelImpl @Inject constructor(
    private val readLoggedUser: ReadLoggedUser,
    private val signIn: SignIn,
    private val routeNavigator: RouteNavigator,
) : ViewModel(), RouteNavigator by routeNavigator {
    init {
        viewModelScope.launch {
            when (val result = readLoggedUser()) {
                is ActionResult.Loading -> {}
                is ActionResult.Error -> {
                    routeNavigator.navigateToRoute(LoginRoute.route)
                }
                is ActionResult.Success -> {
                    logUser(result.data!!)
                }
            }
        }
    }

    private suspend fun logUser(nickname: String) {
        when (signIn(nickname)) {
            is ActionResult.Loading -> {}
            is ActionResult.Error -> {
                routeNavigator.navigateToRoute(LoginRoute.route)
            }
            is ActionResult.Success -> {
                navigateToRoute(UserProfileRoute.get(nickname))
            }
        }
    }
}
