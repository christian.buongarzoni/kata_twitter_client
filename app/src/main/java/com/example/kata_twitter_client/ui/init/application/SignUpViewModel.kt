package com.example.kata_twitter_client.ui.init.application

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.kata_twitter_client.configs.qualifiers.NameValidatorQualifier
import com.example.kata_twitter_client.configs.qualifiers.NicknameValidatorQualifier
import com.example.kata_twitter_client.ui.init.domain.Validator
import com.example.kata_twitter_client.ui.navigation.domain.RouteNavigator
import com.example.kata_twitter_client.ui.user.views.UserProfileRoute
import com.example.model.shared.domain.ActionResult
import com.example.model.user.actions.CreateUser
import com.example.model.user.domain.UserPreferences
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

interface SignUpViewModel{
    val nickname: State<String>
    val nicknameError: State<String?>
    val name: State<String>
    val nameError: State<String?>
    val registrationState: State<RegistrationState>

    fun onEvent(event: SignUpEvent)
}

class PreviewSignUpViewModel : SignUpViewModel {
    override val nickname: State<String> = mutableStateOf("Any nickname")
    override val nicknameError: State<String?> = mutableStateOf("Invalid nickname input")
    override val name: State<String> = mutableStateOf("Any name")
    override val nameError: State<String?> = mutableStateOf("Invalid name input")
    override val registrationState: State<RegistrationState> = mutableStateOf(RegistrationState())

    override fun onEvent(event: SignUpEvent) {/* ignored */}
}

@HiltViewModel
class SignUpViewModelImpl @Inject constructor(
    private val routeNavigator: RouteNavigator,
    @NicknameValidatorQualifier private val validateNickname: Validator,
    @NameValidatorQualifier private val validateName: Validator,
    private val createUser: CreateUser,
    private val userPreferences: UserPreferences,
): ViewModel(), SignUpViewModel, RouteNavigator by routeNavigator {

    private var _nickname = mutableStateOf("")
    override val nickname: State<String> = _nickname

    private var _nicknameError = mutableStateOf<String?>(null)
    override val nicknameError: State<String?> = _nicknameError

    private var _name = mutableStateOf("")
    override val name: State<String> = _name

    private var _nameError = mutableStateOf<String?>(null)
    override val nameError: State<String?> = _nameError

    private var _registrationState = mutableStateOf(RegistrationState())
    override val registrationState: State<RegistrationState> = _registrationState

    override fun onEvent(event: SignUpEvent) {
        when (event) {
            is SignUpEvent.NicknameChanged -> {
                _nickname.value = event.nickname
                executeNicknameValidation()
            }
            is SignUpEvent.NameChanged -> {
                _name.value = event.name
                executeNameValidation()
            }
            is SignUpEvent.OnSubmit -> {
                submitData()
            }
        }
    }

    private fun submitData() {
        executeNicknameValidation()
        executeNameValidation()

        if (requiredFieldsAreNotCompliant()) {
            return
        }

        viewModelScope.launch {
            createUser(nickname = _nickname.value, name = _name.value).collect { result ->
                when (result) {
                    is ActionResult.Loading ->
                        _registrationState.value = RegistrationState(isLoading = true)
                    is ActionResult.Success -> {
                        _registrationState.value = RegistrationState(registrationCompleted = true)
                        userPreferences.createLoggedUser(_nickname.value)
                        navigateToRoute(UserProfileRoute.get(nickname.value))
                    }

                    is ActionResult.Error ->
                        _registrationState.value = RegistrationState(errorMessage = result.message)
                }
            }
        }
    }

    private fun executeNicknameValidation() {
        val nicknameValidationResult = validateNickname(_nickname.value)
        _nicknameError.value = nicknameValidationResult.errorMessage
    }

    private fun executeNameValidation() {
        val nameValidationResult = validateName(_name.value)
        _nameError.value = nameValidationResult.errorMessage
    }

    private fun requiredFieldsAreNotCompliant() =
        (_nicknameError.value != null || _nameError.value != null)
}
