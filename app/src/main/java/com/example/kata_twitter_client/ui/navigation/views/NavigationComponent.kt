package com.example.kata_twitter_client.ui.navigation.views

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import com.example.kata_twitter_client.ui.init.views.LoginRoute
import com.example.kata_twitter_client.ui.init.views.SignUpRoute
import com.example.kata_twitter_client.ui.init.views.SplashRoute
import com.example.kata_twitter_client.ui.user.views.UserProfileRoute
import com.google.accompanist.navigation.animation.AnimatedNavHost

@ExperimentalAnimationApi
@Composable
fun NavigationComponent(navHostController: NavHostController, paddingValues: PaddingValues) {
    AnimatedNavHost(
        navController = navHostController,
        startDestination = SplashRoute.route,
        modifier = Modifier.padding(paddingValues)
    ) {
        SplashRoute.composable(this, navHostController)
        LoginRoute.composable(this, navHostController)
        SignUpRoute.composable(this, navHostController)
        UserProfileRoute.composable(this, navHostController)
    }
}
