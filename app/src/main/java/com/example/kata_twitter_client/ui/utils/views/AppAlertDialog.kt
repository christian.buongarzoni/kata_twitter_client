package com.example.kata_twitter_client.ui.utils.views

import androidx.compose.material.AlertDialog
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import com.example.kata_twitter_client.R

@Composable
fun AppAlertDialog(title: String, errorMessage: String, onDismiss: () -> Unit = {}, onConfirm: () -> Unit = {}) {
    val openDialog = remember { mutableStateOf(true) }
    if (openDialog.value)
    {
        AlertDialog(
            onDismissRequest = { openDialog.value = false
            onDismiss() },
            title = { Text(text = title, color = Color.Black) },
            text = { Text(text = errorMessage, color = Color.Black) },
            confirmButton = {
                AppButton(
                    text = stringResource(id = R.string.confirm),
                    onClick = {
                        openDialog.value = false
                        onConfirm()
                    }
                )
            },
        )
    }
}
