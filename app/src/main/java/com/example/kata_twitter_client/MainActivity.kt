package com.example.kata_twitter_client

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.material.Scaffold
import com.example.kata_twitter_client.ui.navigation.views.NavigationComponent
import com.example.kata_twitter_client.ui.theme.Kata_Twitter_ClientTheme
import com.google.accompanist.navigation.animation.rememberAnimatedNavController
import dagger.hilt.android.AndroidEntryPoint

@ExperimentalAnimationApi
@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val navController = rememberAnimatedNavController()
            Kata_Twitter_ClientTheme {
                Scaffold {
                    NavigationComponent(navController, it)
                }
            }
        }
    }
}
