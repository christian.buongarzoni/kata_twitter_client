package com.example.kata_twitter_client.configs.qualifiers

import javax.inject.Qualifier

@Qualifier
annotation class NicknameValidatorQualifier
