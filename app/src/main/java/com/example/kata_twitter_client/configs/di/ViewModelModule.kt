package com.example.kata_twitter_client.configs.di

import com.example.kata_twitter_client.configs.qualifiers.NameValidatorQualifier
import com.example.kata_twitter_client.configs.qualifiers.NicknameValidatorQualifier
import com.example.kata_twitter_client.ui.init.domain.ValidateName
import com.example.kata_twitter_client.ui.init.domain.ValidateNickname
import com.example.kata_twitter_client.ui.init.domain.Validator
import com.example.kata_twitter_client.ui.navigation.domain.RouteNavigator
import com.example.kata_twitter_client.ui.navigation.domain.RouteNavigatorImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped

@Module
@InstallIn(ViewModelComponent::class)
abstract class ViewModelModule {
    @ViewModelScoped
    @Binds
    abstract fun bindRouteNavigator(routeNavigatorImpl: RouteNavigatorImpl): RouteNavigator

    @ViewModelScoped
    @NameValidatorQualifier
    @Binds
    abstract fun bindNameValidator(validateName: ValidateName): Validator

    @ViewModelScoped
    @NicknameValidatorQualifier
    @Binds
    abstract fun bindNicknameValidator(validateNickname: ValidateNickname): Validator
}
