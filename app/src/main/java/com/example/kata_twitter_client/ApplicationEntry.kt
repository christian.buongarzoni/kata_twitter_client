package com.example.kata_twitter_client

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class ApplicationEntry : Application() {
}
