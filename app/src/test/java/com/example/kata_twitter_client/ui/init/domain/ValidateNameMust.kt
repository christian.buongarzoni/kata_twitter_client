package com.example.kata_twitter_client.ui.init.domain

import com.example.kata_twitter_client.shared.domain.StringObjectMother
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

class ValidateNameMust {
    private lateinit var validateName: ValidateName

    @Before
    fun setup() {
        validateName = ValidateName()
    }

    @Test
    fun `result on error when name is empty`() {
        val validationResult = validateName("  ")

        assertFalse(validationResult.isSuccessful)
        assertEquals("Please add a name", validationResult.errorMessage)
    }

    @Test
    fun `result on error when name contains an invalid character`() {
        val validationResult = validateName(StringObjectMother.alphabeticRandom(10) + ".")

        assertFalse(validationResult.isSuccessful)
        assertEquals(
            "Name must contain only alphabetic characters and spaces",
            validationResult.errorMessage
        )
    }

    @Test
    fun `result on error when name is longer than 50 characters`() {
        val validationResult = validateName(StringObjectMother.alphabeticRandom(51))

        assertFalse(validationResult.isSuccessful)
        assertEquals("Name must be up to 50 characters", validationResult.errorMessage)
    }

    @Test
    fun `result successful when the name contains valid characters`() {
        val validationResult = validateName(
            StringObjectMother.alphabeticRandom(40) +
                    " " + StringObjectMother.alphabeticRandom(9)
        )

        assertTrue(validationResult.isSuccessful)
    }
}
