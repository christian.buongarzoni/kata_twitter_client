package com.example.kata_twitter_client.ui.init.domain

import com.example.kata_twitter_client.shared.domain.StringObjectMother
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

class ValidateNicknameMust {

    private lateinit var validateNickname: ValidateNickname

    @Before
    fun setup() {
        validateNickname = ValidateNickname()
    }

    @Test
    fun `result on error when nickname is empty`() {
        val validationResult = validateNickname("")

        assertFalse(validationResult.isSuccessful)
        assertEquals("Please add a nickname", validationResult.errorMessage)
    }

    @Test
    fun `result on error when nickname contains an invalid character`() {
        val validationResult = validateNickname(StringObjectMother.lowercaseRandom(4) + "*")

        assertFalse(validationResult.isSuccessful)
        assertEquals("Nickname must contain only lowercase letters, numbers, dots and underscores", validationResult.errorMessage)
    }

    @Test
    fun `result on error when nickname is longer than 21 characters`() {
        val validationResult = validateNickname(StringObjectMother.lowercaseRandom(22))

        assertFalse(validationResult.isSuccessful)
        assertEquals("Nickname must be up to 21 characters", validationResult.errorMessage)
    }

    @Test
    fun `result successful when the nickname contains valid characters`() {
        val validationResult = validateNickname(StringObjectMother.lowercaseRandom(19) + "_" + ".")

        assertTrue(validationResult.isSuccessful)
    }
}
