object Hilt {
    private const val hiltVersion = "2.43"
    const val android = "com.google.dagger:hilt-android:$hiltVersion"
    const val compiler = "com.google.dagger:hilt-compiler:$hiltVersion"
    const val androidCompiler = "com.google.dagger:hilt-compiler:$hiltVersion"
}
