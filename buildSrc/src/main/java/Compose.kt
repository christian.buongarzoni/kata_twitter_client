object Compose {
    const val version = "1.1.1" //  1.2.0-rc02 ?
    const val activity = "androidx.activity:activity-compose:1.3.1"
    const val hiltNavigation = "androidx.hilt:hilt-navigation-compose:1.0.0"
    const val material = "androidx.compose.material:material:$version"
    const val navigation = "androidx.navigation:navigation-compose:2.5.0-rc02"
    const val ui = "androidx.compose.ui:ui:$version"
    const val uiToolingPreview = "androidx.compose.ui:ui-tooling-preview:$version"
}