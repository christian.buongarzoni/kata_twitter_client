object Retrofit {
    const val converterGson = "com.squareup.retrofit2:converter-gson:2.9.0"
    const val okHttp = "com.squareup.okhttp3:okhttp:5.0.0-alpha.10"
    const val retrofit = "com.squareup.retrofit2:retrofit:2.9.0"
}
