object AndroidX {
    const val appCompat = "androidx.appcompat:appcompat:1.4.2"
    const val coreKtx = "androidx.core:core-ktx:1.7.0"
    const val dataStorePreferences = "androidx.datastore:datastore-preferences:1.0.0"
    const val lifecycleRuntimeKtx = "androidx.lifecycle:lifecycle-runtime-ktx:2.3.1"
}
